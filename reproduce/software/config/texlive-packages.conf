# Necessary packages to install in TeX Live.
#
# If any extra TeX package is necessary to build your paper, just add its
# name to this variable (you can check in 'ctan.org' to find the official
# name).
#
# Copyright (C) 2018-2025 Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Copyright (C) 2022-2025 Boud Roukema <boud@astro.uni.torun.pl>
#
# Copying and distribution of this file, with or without modification, are
# permitted in any medium without royalty provided the copyright notice and
# this notice are preserved.  This file is offered as-is, without any
# warranty.

# Hints:
#
# - For debugging: after a partial or successful build, look through
#   '.build/software/installed/texlive/maneage/tlpkg/texlive.tlpdb.main.*'
#   to see what packages and files were looked at during the install.

# Notes:
#
# - tex and fancyhdr: These two packages are installed along with the basic
#   installation scheme that we used to install tlmgr, they will be ignored
#   in the 'tlmgr install' command, but will be used later when we want
#   their versions.
#
# - fancyvrb: needed by R.
texlive-packages = biber \
                   biblatex \
                   caption \
                   courier \
                   csquotes \
                   datetime \
                   fancyvrb \
                   fmtcount \
                   fontaxes \
                   footmisc \
                   fp \
                   kastrup \
                   logreq \
                   mweights \
                   newtx \
                   pgf \
                   pgfplots \
                   preprint \
                   setspace \
                   tex-gyre \
                   times \
                   titlesec \
                   trimspaces \
                   txfonts \
                   ulem \
                   xcolor \
                   xkeyval \
                   xpatch \
                   xstring
